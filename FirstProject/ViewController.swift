//
//  ViewController.swift
//  FirstProject
//
//  Created by Tran Huu Luong on 5/26/19.
//  Copyright © 2019 Tran Huu Luong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lbAsama: UILabel!
    @IBOutlet weak var txtKQ: UILabel!
    
    @IBOutlet weak var imgNNC: UIImageView!
    @IBAction func btnLogin(_ sender: Any) {
        print("Tai khoan la: \(edtUserName.text!)")
        txtKQ.numberOfLines = 0
        txtKQ.text = "Xin chao \(edtUserName.text!)"
    }
    @IBOutlet weak var edtPass: UITextField!
    @IBOutlet weak var edtUserName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        lbAsama.textColor = UIColor.red;
        lbAsama.layer.borderColor = UIColor.blue.cgColor
        edtPass.isSecureTextEntry = true
        edtPass.placeholder = "Nhap mat khau"
        edtUserName.placeholder = "Nhap username"
        imgNNC.image = UIImage(named: "nnc")
        // Do any additional setup after loading the view.
        
    }
}

